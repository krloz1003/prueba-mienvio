<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Producto;

class ProductoFactory extends Factory
{
    protected $model = Producto::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre'        => $this->faker->unique()->name,
            'descripcion'   => $this->faker->text,
            'precio'        => $this->faker->randomDigit,
        ];
    }
}
