<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Producto;

class ProductoController extends Controller
{
    public function index(){
        return view('inicio');
    }

    public function rows(Request $request){        
        $q = Producto::select('id', 'nombre', 'descripcion', 'precio');
        if ($request->search) {
            $q->where('nombre','LIKE', '%'.$request->search.'%');
            $q->orWhere('descripcion','LIKE', '%'.$request->search.'%');
        }
        if ($request->pricemin && $request->pricemax) {
            $q->whereBetween('precio', [$request->pricemin, $request->pricemax]);
        }
        $q->orderBy('nombre');
        $rows = $q->paginate();
        return response()->json($rows);
    }

    public function store(Request $request){

        $validator = $this->validateForm($request);
        if($validator->fails()){
            return response()->json($validator->messages(), 422);
        }

        $row = new Producto;
        $row->nombre = $request->nombre;
        $row->descripcion = $request->descripcion;
        $row->precio = $request->precio;
        $row->save();

        return response()->json(['message' => 'Los datos se guardaron correctamente.'],200);

        
    }

    private function validateform($request){
        return \Validator::make($request->all(), [
            'nombre'        => 'required',
            'descripcion'   => 'required',
            'precio'        => 'required|regex:/^\d+(\.\d{1,2})?$/',
		]);
    }

}
