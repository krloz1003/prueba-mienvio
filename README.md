# Instrucciones para ejecutar la aplicación

1. Clonar el proyecto: <code>git clone https://gitlab.com/krloz1003/prueba-mienvio.git </code>
2. Ingresar a la carpeta del proyecto: <code>cd prueba-mienvio</code>
3. Crear base de datos del proyecto: <code>db_mienvio</code>
4. Configurar el archivo de variables entorno: <code>cp .env.example .env</code> 
<pre>
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_mienvio
DB_USERNAME=root
DB_PASSWORD=password
</pre>
5. Descargar dependencias: <code>composer update</code>
6. Crear llave cifrado: <code>php artisan key:generate</code>
7. Crear y poblar tablas: <code>php artisan migrate:fresh --seed</code>
8. Iniciar el servidor de pruebas: <code>php artisan serve</code>
9. En el navegador Ingresamos a la ruta: http://localhost:8000/ o http://127.0.0.1:8000/
10. Listo!!! ahora puedes probar la aplicación.

## Usando el zip adjunto

1. Descargar los archivos en la ubicación de trabajo.
2. Ingresar a la carpeta del proyecto: <code>cd prueba-mienvio</code>
3. Continuar con el **paso 3** de las instrucciones anteriores.